""" Brute force routine to compute angular separations between photon events in
    Fermi data. """

import numpy as np
from astropy.io import fits
from datetime import datetime
import sys
import os
import traceback
import multiprocessing

MAX_ANG = 5.
FERMI_DTYPE = [ ('ENERGY', '2f4'), ('RA', '2f4'), ('DEC', '2f4'), ('L', '2f4'), 
                ('B', '2f4'), ('ZENITH_ANGLE', '2f4'), ('TIME', '2f8'), 
                ('EVENT_ID', '2i4') ]  # Should I include event type/class ?

def get_ang_dist(ra, dec):
    """ This function computes the separation between two events using a 
        quadratic approximation to the great circle formula.

        Parameters
        ------------
        ra: (float, float)
            The RA coordinate of the first and second events. (degrees)
        dec: (float, float)
            The declination coordinate of the first and second events. (degrees)

        Returns
        --------
        float or None
            The angular separation (in degrees) or None if the difference in dec
            was greater than maximum specified at top of file.
    """
    d_dec = np.abs(dec[1] - dec[0])
    if d_dec > MAX_ANG:
        return None
    
    d_ra = np.abs(ra[1] - ra[0])
    if d_ra > 180.:
        d_ra = 360. - d_ra
    
    # Use exact formula near poles
    if 90 - np.abs(dec[0]) < 10. or 90 - np.abs(dec[1]) < 10.:
        return np.degrees(np.arccos( np.sin(np.radians(dec[0]))*np.sin(np.radians(dec[1])) +\
                              np.cos(np.radians(dec[0]))*np.cos(np.radians(dec[1]))*np.cos(np.radians(d_ra)) ))
    else:
        return np.sqrt(d_dec**2 + d_ra**2 * np.cos(np.radians(dec[0])) * np.cos(np.radians(dec[1])))

def brute_force_wrapper(args):
    """ Wrapper function to pass arguments to brute_force when running in 
        multiprocessing.Pool. """
    return brute_force(*args)

def brute_force(events, ind, fname, job_ind=0):
    """ Loop through every pair of events with supplied RA and declination and 
        compute their angular separations, skipping those with differences >MAX_ANG 
        degrees in declination and auto-differences.

        Parameters
        ------------
        events: np.recarray
            Mulicolumn array of *all* events, the subset defined by 'ind' of 
            which will be used as first element for pair distance computations.
        ind: list of int
            Indices of first of pair of events to be considered by this job.
        fname: string
            Filename to save results to, in FITS format.
        job_ind: int
            The index associated with this job if the full calculation has been 
            split into multiple parallel jobs.

        Returns
        ---------
        list of list: [ float, (int, int) ]
            Array of angular distances and pairs of event IDs.
        0 or Exception:
            If an exception occurs in the main loop it will be passed on along 
            with the results up to that point.
    """
    # Create FITS file
    # Create binary HDU; J: i4, E: f4  Don't think this is necessary. Try without
    #tbhdu = fits.BinTableHDU.from_columns([ fits.Column(name='dist', format='E'), 
    #                                         fits.Column(name='evid', format='2J')])
    # Create header
    prihdr = fits.Header()
    prihdr['JOB_IND'] = job_ind
    prihdr['COMMENT'] = "Results of angular distance computation for pairs of "+\
                        "events above 50 GeV (for job {:d}).".format(job_ind)
    # Add header and table to list, and create file
    thdulist = fits.HDUList([fits.PrimaryHDU(header=prihdr)])
    thdulist.writeto('{}.fits'.format(fname))

    # Load individual arrays
    ev_ra = events['RA']
    ev_dec = events['DEC']
    ev_en = events['ENERGY']
    ev_l = events['L']
    ev_b = events['B']
    ev_zen = events['ZENITH_ANGLE']
    ev_t = events['TIME'] 
    ev_id = events['EVENT_ID']

    print "\nStarting job {:d}.".format(job_ind)
    try:
        # Append results to FITS file every 10000000 counts 
        # (10e6 * 76 bytes ~ 750 MB)
        arr_size = 10e6
        distances = np.empty(arr_size, dtype=[('dist','f4')] + FERMI_DTYPE)
        k = 0  # Total result counter
        arr_ind = 0  # Index of results array
        ev_len = len(ev_id)
        # Loop over every pair
        for i in ind:
            for j in range(i+1, ev_len):  # Skip autos
                ra = (ev_ra[i], ev_ra[j])
                dec = (ev_dec[i], ev_dec[j])
                new_dist = get_ang_dist(ra, dec)
                if new_dist is not None and new_dist <= MAX_ANG:  # Don't save if >MAX_ANG
                    distances[arr_ind] = (new_dist, [ev_en[i], ev_en[j]], 
                                          [ev_ra[i], ev_ra[j]], 
                                          [ev_dec[i], ev_dec[j]], 
                                          [ev_l[i],ev_l[j]], [ev_b[i],ev_b[j]],
                                          [ev_zen[i], ev_zen[j]], 
                                          [ev_t[i], ev_t[j]], 
                                          [ev_id[i], ev_id[j]])
                    k += 1  # Increment total
                    arr_ind += 1  # Increment results array cell
                if arr_ind == arr_size:  # Array is full: write to file
                    fits.append('{}.fits'.format(fname), data=distances, verify=False)
                    # Reset array
                    arr_ind = 0
        # Write last, partially filled, results array
        fits.append('{}.fits'.format(fname), data=distances[:arr_ind], verify=False)

        print "\nJob {:d}: Done!".format(job_ind)
    except Exception as e:
        print "\nJob {:d}: An exception occured:".format(job_ind)
        traceback.print_exc()
        print "\nJob {:d}: Computed {:d} angular distances.".format(job_ind, k)
        # Write last results
        fits.append('{}.fits'.format(fname), data=distances[:arr_ind], verify=False)
        print "\nJob {:d}: Results saved to {}.".format(job_ind, '{}.fits'.format(fname))
        return k, e

    print "\nJob {:d}: Computed {:d} angular distances.".format(job_ind, k)
    print "Job {:d}: Results saved to {}.".format(job_ind, '{}.fits'.format(fname))
    return k, None

if __name__ == '__main__':
    """ Main routine. Get input and output paths from arguments and split 
        computation into a given number of jobs. 

        Requires first argument to be the input events FITS file, and will use 
        second argument as output filename, if specified. """
    args = sys.argv
    if len(args) <= 1:
        print "Must supply path to events FITS file."
    elif not os.path.isfile(args[1]):
        print "File '{}' does not exist.".format(args[1])
    else:
        # Read file
        evfile = args[1]
        print "\nReading from events file '{}'".format(evfile)
        events = np.array(fits.open(evfile)['EVENTS'].data)
        ev_len = events.shape[0]
        print "\nWill compute distances for pairs from {:d} events\n".format(ev_len)
        # Set up jobs
        job_num = 8
        errors = []
        iter_per_job = ev_len / job_num
        pool = multiprocessing.Pool(processes=job_num)  # Process pool
        # Distribute indices evenly
        base_ind = np.arange(0, ev_len - job_num, job_num)
        indices = [ base_ind + i for i in range(job_num) ]
        indices[0] = np.concatenate( (indices[0], 
                                      np.arange(indices[-1][-1]+1, ev_len)) ) 
        del base_ind

        # Parse output filename
        if len(args) > 2:
            basename = os.path.splitext(args[2])[0]
        else:
            basename = "distances_{}".format(datetime.now().strftime("%d%m%Y_%H%M"))
        fnames = [ "{}_{:d}".format(basename, i) for i in range(job_num) ]

        proc_args = [ (events, indices[i], fnames[i], i) for i in range(job_num) ]
        #   Note: passing the same arrays to each job should not result in 
        #   multiple copies in memory on linux (I think).
        # Run jobs
        job_out = pool.map(brute_force_wrapper, proc_args)
        counts = [x[0] for x in job_out]
        errors = [x[1] for x in job_out]

        # Merge all results into one file
        # Create header
        prihdr = fits.Header()
        prihdr['COMMENT'] = "Results of angular distance computation for pairs of "+\
                            "events above 50 GeV, with separation less than" +\
                            " {:.2f} deg.".format(MAX_ANG)
        prihdr['COMMENT'] = "Generated by Tristan Pinsonneault-Marotte, for " +\
                            "the course PHYS 459 at McGill university, under " +\
                            "the supervision of Prof. Ken Ragan."
        prihdr['DATE'] = str(datetime.now())
        # Merge tables from all files
        merged_res = np.concatenate([ part.data for f in fnames for part in 
                                    fits.open(f + '.fits')[1:] ])
        thdulist = fits.HDUList([ fits.PrimaryHDU(header=prihdr), 
                                  fits.BinTableHDU(merged_res) ])
        # Write to file
        thdulist.writeto('{}.fits'.format(basename))

        # *TODO: Remove indiviual job files?

        # Save result counts
        np.savez('{}_counts.npz'.format(basename), counts=np.array(counts))

        # Check for exceptions in previous steps
        for i, error in enumerate(errors):
            if error is not None:
                print "\nJob {:d} ran into an exception:"
                print error

