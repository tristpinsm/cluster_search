"""" This script reads a fits file and sorts its content into seperate files that
    correspond to a partitioning of the celestial sphere. 
    (roughly modeled after 'mask_gal.py') """
from astropy.io import fits
import numpy as np
import sys
import os
import multiprocessing

def sort_ev(events, ind, cells, job_i=0):
    """ Sort events into cells and return sorted indices. """
    print "\nJOB {:d} Begin sorting...".format(job_i)
    # Sorting loop
    sorted_ind = [ [] for n in range(len(cells)) ]
    orphan_ind = []
    for i in ind:
        ev_l = events['L'][i]
        ev_b = events['B'][i]
        found_cell = False
        for n in range(len(cells)):
            if cells['L'][n][1] < cells['L'][n][0]:
                if (ev_l > cells['L'][n][0] or ev_l < cells['L'][n][1]) and (
                    ev_b > cells['B'][n][0] and ev_b < cells['B'][n][1]):
                    sorted_ind[n].append(i)
                    found_cell = True
                    break
            else:
                if (ev_l > cells['L'][n][0] and ev_l < cells['L'][n][1]) and (
                    ev_b > cells['B'][n][0] and ev_b < cells['B'][n][1]):
                    sorted_ind[n].append(i)
                    found_cell = True
                    break
        if not found_cell: orphan_ind.append(i)

    print "JOB {:d} Sorting completed.".format(job_i)
    print "JOB {:d} {:d} events were not associated with any cell.".format(job_i, len(orphan_ind))
    return sorted_ind, orphan_ind

def sort_ev_wrapper(args):
    """ Wrapper for passing arguments to sort_ev for multiprocesses. """
    return sort_ev(*args)

if __name__ == "__main__":
    """ Main routine.
        First part defines the partitioning that will be employed.
        The input file and prefix for output filename are read from command line 
        arguments. The set of cells is divided into a given number of jobs.
    """
    # Parse input and output files
    args = sys.argv
    if len(args) <= 2:
        raise Exception("Must supply path to events FITS file and output name prefix.")
    elif not os.path.isfile(args[1]):
        raise Exception("File '{}' does not exist.".format(args[1]))
    infile = args[1]
    outfile = args[2]
    if not os.path.isdir(os.path.dirname(args[2])):
        os.makedirs(os.path.dirname(args[2]))
    if len(args) <= 3:
        overlap = False
    elif args[3] == "overlap":
        print "\nWill sort into set of overlap cells."
        overlap = True
    else:
        raise Exception("Unable to parse third argument. Aborting")

    # Partition in L and B to get roughly same number of events in each patch
    # Perform a second partitioning with overlap if specified
    if overlap:  # Put overlapping partition definition here
        l_edges = [ (0.,360.), (90.,270.,90.), np.concatenate((np.linspace(30, 
                330, 6, endpoint=True), (30.,))), (90.,270.,90.), (0.,360.) ]
        b_edges = [ -90., -35., -5., 5., 35., 90. ]
    else:
        l_edges = [ (0.,360.), (0.,180.,360.), np.linspace(0, 360, 7, endpoint=True), 
                    (0.,180.,360.), (0.,360.) ]
        b_edges = [ -90., -45., -10., 10., 45., 90. ]
    n_cells = sum([len(r)-1 for r in l_edges])
    cells = np.zeros(n_cells, dtype=[('L', '2f'), ('B', '2f')])
    k = 0
    for b in range(len(b_edges)-1):
        for l in range(len(l_edges[b])-1):
            cells['L'][k] = (l_edges[b][l], l_edges[b][l+1])
            cells['B'][k] = (b_edges[b], b_edges[b+1])
            k += 1

    # Read file
    print "\nReading from events file '{}'".format(infile)
    infits = fits.open(infile)
    prihdu = infits['PRIMARY']
    prihdu.header['FILENAME'] = os.path.split(outfile)[-1]
    gti = infits['GTI']
    events = np.array(infits['EVENTS'].data)
    ev_len = events.shape[0]

    # Split events among jobs
    job_num = 8
    base_ind = np.arange(0, ev_len - job_num, job_num)
    indices = [ base_ind + i for i in range(job_num) ]
    indices[0] = np.concatenate( (indices[0], 
                                  np.arange(indices[-1][-1]+1, ev_len)) ) 
    del base_ind
    
    # Start jobs
    pool = multiprocessing.Pool(processes=job_num)  # Process pool
    proc_args = [ (events, indices[i], cells, i) for i in range(job_num) ]
    #   Note: passing the same arrays to each job should not result in 
    #   multiple copies in memory on linux (I think).
    # Run jobs
    job_out = pool.map(sort_ev_wrapper, proc_args)

    # Save results
    sorted_events = [ np.concatenate([ events[x[0][n]] for x in job_out ]) for 
                      n in range(n_cells) ]
    orphan_events = np.concatenate([ events[x[1]] for x in job_out ])
    for n in range(n_cells):
        # Create new events table
        new_events = fits.BinTableHDU(sorted_events[n])
        new_events.header = infits['EVENTS'].header
        new_events.name = infits['EVENTS'].name
        # Record in file history
        new_events.header['HISTORY'] = ("Restricted to events in region given" +
                        "by L in {} and B in {}.").format(str(cells['L'][n]), 
                                                          str(cells['B'][n]))
        # Write to file
        hdulist = fits.HDUList( [prihdu, new_events, gti] )
        fname = outfile + "_{:d}_{:.0f}_{:.0f}.fits".format(n, cells['B'][n][0],
                                                            cells['L'][n][0])
        hdulist.writeto(fname)
        print "Wrote cell {:d} events to file {}".format(n, fname)
