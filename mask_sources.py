""" This script reads a fits file and outputs a modified version with specified 
    point sources masked according to the instrument PSF. """
from astropy.io import fits
import numpy as np
import sys
import os
from brute_force import get_ang_dist

def mask_sources(infile, outfile, sources, psf_energy, psf_frac, psf_file, comments=None):
    """ Make a copy of 'infile' with array of 'sources' (columns 'RA' and 'DEC') 
    masked according to the PSF containment 'psf_frac' read from 'psf_file' at 
    'psf_energy' (MeV) and save to 'outfile' """
    print "\nReading from {}".format(infile)
    # Read input file, copy header 
    infits = fits.open(infile)
    prihdu = infits['PRIMARY']
    prihdu.header['FILENAME'] = os.path.split(outfile)[-1]
    gti = infits['GTI']
    events = infits['EVENTS']
    new_data = fits.fitsrec.FITS_rec.from_columns(events.columns, fill=True)  # Fill with zeros
    # Get psf constraint
    psf_radius = get_psf(psf_file, psf_energy, psf_frac)
    # Filter events
    print "\nFiltering events..."
    k = 0  # initialize counter
    for i in range(events.data.shape[0]):
        for j in range(sources.shape[0]):
            near_source = False
            # Compute distance from source. Returns None if less than MAX_ANG (5 deg)
            dist = get_ang_dist( (events.data['RA'][i], sources['RA'][j]), 
                (events.data['DEC'][i], sources['DEC'][j]) )
            if dist is not None and dist < psf_radius:
                near_source = True
                break
        if not near_source:
            new_data[k] = events.data[i]
            k += 1
    print "Masking completed."
    print "{:d} events remain of the initial {:d}".format(k, events.data.shape[0])
    # Discard empty cells
    new_data = new_data[:k]
    # Create new events table
    new_events = fits.BinTableHDU(new_data)
    new_events.header = events.header
    new_events.name = events.name
    # Record this edit to file history
    new_events.header['HISTORY'] = (r"Masked a selection of point sources, to a radius " +
                                    "of {:.3f} deg ({:.0f}% PSF at {:.0f}GeV)."
                                    ).format(psf_radius, psf_frac*100, psf_energy/1e3)
    new_events.header['COMMENT'] = r"Masked sources at: {}".format(str(sources).encode("string-escape"))
    if comments is not None:
        new_events.header['COMMENT'] = "{}".format(comments.encode("string-escape"))
    # Write to file
    hdulist = fits.HDUList( [prihdu, new_events, gti] )
    hdulist.writeto(outfile)

    print "\nSaved new file {}".format(outfile)

def get_psf(psffile, energ, fraction, check_energy=True):
    """ Get the angular radius containing 'fraction' of counts at energy 
        'energ' from the 'psffile' """
    # Open file
    psf = fits.open(psffile)['PSF'].data
    thetas = fits.open(psffile)['THETA'].data['Theta']
    # Compute width of bins
    theta_bins = np.zeros(thetas.shape[0])
    theta_bins[1:-1] = (thetas[2:] - thetas[:-2]) / 2.
    theta_bins[0] = (thetas[1] - thetas[0]) / 2.
    theta_bins[-1] = (thetas[-1] - thetas[-2]) / 2.
    # Find energy index and compute area under PSF
    ind_energy = np.argmin(np.abs(psf['ENERGY'] - energ))
    if check_energy and (psf['ENERGY'][ind_energy] - energ) / energ > 0.10:
        raise Exception("Closest energy bin in file is {:.2f}".format(psf['ENERGY'][ind_energy]) +
            ", more than 10% from what was requested. Aborting.\nIf you want to " +
            "proceed anyways, set the 'check_energy' flag to False.")
    area = np.sum( np.sin(np.radians(thetas)) * theta_bins * psf['Psf'][ind_energy] )
    # Find containment angle for 'fraction'
    area_sofar = 0
    i = 0
    while area_sofar / area <= fraction:
        i += 1
        area_sofar += np.sin(np.radians(thetas[i])) * theta_bins[i] * psf['Psf'][ind_energy][i]
    return thetas[i]

if __name__ == "__main__":
    """ Mask the sources listed in a local file. """
    print "Masking bright 2FHL sources."
    src_table = np.loadtxt("./fhl_srcs.txt", skiprows=11, usecols=(2,3))
    sources = np.zeros(len(src_table), dtype=[('RA','f'), ('DEC','f')])
    sources['RA'] = src_table[:,0]
    sources['DEC'] = src_table[:,1]
    mask_sources(sys.argv[1], sys.argv[2], sources, 50000., 0.99, sys.argv[3], 
                 comments="Masked 2FHL point sources with flux at least 10% that of the Crab.")

    """
    print "Debug run: masking Crab nebula."
    sources = np.zeros(1, dtype=[('RA', 'f'), ('DEC', 'f')])
    sources[0] = (83.6332, 22.0145)  # Crab
    mask_sources(sys.argv[1], sys.argv[2], sources, 50000., 0.99, sys.argv[3])
    """


