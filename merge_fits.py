from astropy.io import fits
import numpy as np
import sys

def merge_fits(fnames, outfile, pri_header=None):
    """ Merge FITS files in list fnames. 
        Merges all HDUs into one. Assumes all tables have same columns.
        If no primary header is specified, will just cpoy the one from the first file. """
    # Get header
    if pri_header is None:
        print "Using header from {}.".format(fnames[0])
        pri_header = fits.open(fnames[0])['PRIMARY'].header
        pri_header['COMMENT'] = "File genererated by merging {}.".format(
                                str(fnames).encode("string-escape"))
    # Merge tables from all files
    merged_dat = np.concatenate([ part.data for f in fnames for part in 
                                fits.open(f)[1:] ])
    thdulist = fits.HDUList([ fits.PrimaryHDU(header=pri_header), 
                              fits.BinTableHDU(merged_dat) ])
    # Write to file
    thdulist.writeto(outfile)
    print "Merged {:d} files into {}.".format(len(fnames), outfile)

if __name__ == "__main__":
    args = sys.argv
    if len(args) < 3:
        raise Exception("Must supply at least an input and output file.")
    infiles = args[1:-1]
    outfile = args[-1]
    merge_fits(infiles, outfile)
