""" Routine to compute angular separations between triplets of photon events in
    Fermi data. Based off the routine for pairs 'brute_force.py' 

    The full iterative routine for the clustering serach is implemented in the 
    script 'iterate_clustering.py' using functions defined here. """

import numpy as np
from astropy.io import fits
from datetime import datetime
import sys
import os
import traceback
import multiprocessing
from mask_sources import get_psf

# Upper limit on distances that will be saved
MAX_ANG = 0.3
# DTYPE that mimicks fields from the Fermi data products
FERMI_DTYPE = [ ('RA', 'f4'), ('DEC', 'f4'), ('ENERGY', '3f4'),
                ('ZENITH_ANGLE', '3f4'), ('TIME', '3f8'), 
                ('EVENT_ID', '3i4') ]  # Should I include event type/class ?
FERMI_DTYPE_ALT = [ ('RA', 'f4'), ('DEC', 'f4'), ('ENERGY', 'f4'), ('EVENT_ID', 'i4') ]

def get_ang_dist(ra, dec):
    """ This function computes the separation between two events using a 
        quadratic approximation to the great circle formula.

        Parameters
        ------------
        ra: (float, float)
            The RA coordinate of the first and second events. (degrees)
        dec: (float, float)
            The declination coordinate of the first and second events. (degrees)

        Returns
        --------
        float or None
            The angular separation (in degrees) or None if the difference in dec
            was greater than maximum specified at top of file.
    """
    d_dec = np.abs(dec[1] - dec[0])
    if d_dec > MAX_ANG:
        return None
    
    d_ra = np.abs(ra[1] - ra[0])
    if d_ra > 180.:
        d_ra = 360. - d_ra
    
    # Use exact formula near poles
    if 90 - np.abs(dec[0]) < 10. or 90 - np.abs(dec[1]) < 10.:
        return np.degrees(np.arccos( np.sin(np.radians(dec[0]))*np.sin(np.radians(dec[1])) +\
                              np.cos(np.radians(dec[0]))*np.cos(np.radians(dec[1]))*np.cos(np.radians(d_ra)) ))
    else:
        return np.sqrt(d_dec**2 + d_ra**2 * np.cos(np.radians(dec[0])) * np.cos(np.radians(dec[1])))

def weighted_scale(dist, energ, psf_model):
    """ PSF weighted mean of distances.

    Parameters
    -----------
    dist: (float, float, float)
        Pairwise distances: ij, ik, jk
    energ: (float, float, float)
        Energy of evenrts i, j, k (must match ordering of dist)
    psf_model: np.recarray( [('ENERGY', float), ('WEIGHTS', float)] )
        Standard deviation of PSF for a set of energies.

    Returns
    ---------
    float: weighted scale of triplet
    """
    psf_w = np.array([ psf_model['WEIGHTS'][np.argmin(np.abs(psf_model['ENERGY'] - e))] for e in energ])
    return np.sum( psf_w * dist ) / np.sum(psf_w)

def weighted_coord(ra, dec, energ=None, psf_model=None):
    """ PSF weighted coordinates of triplet.

    Parameters
    -----------
    ra, dec: (float, float, float)
        Coordinates of events i, j, k
    energ: (float, float, float)
        Energy of events i, j, k (must match ordering above)
    psf_model: np.recarray( [('ENERGY', float), ('WEIGHTS', float)] )
        Standard deviation of PSF for a set of energies.

    Returns
    ---------
    (float, float): weighted coordinates of triplet
    """
    if energ is None or psf_model is None:
        psf_w = np.ones(len(ra))
    else:
        psf_w = np.array([ psf_model['WEIGHTS'][np.argmin(np.abs(psf_model['ENERGY'] - e))] for e in energ])
    # convert to cartesian and find mean
    cos_dec = np.cos(np.radians(dec))
    x = np.sum(psf_w * cos_dec * np.cos(np.radians(ra))) / np.sum(psf_w)
    y = np.sum(psf_w * cos_dec * np.sin(np.radians(ra))) / np.sum(psf_w)
    z = np.sum(psf_w * np.sin(np.radians(dec))) / np.sum(psf_w)
    
    return ( (np.degrees(np.arccos(x / np.sqrt(x**2+y**2))) if y >= 0. else 360. - 
            np.degrees(np.arccos(x / np.sqrt(x**2+y**2)))), 
            np.degrees(np.arctan(z / np.sqrt(x**2+y**2))) )

def get_psf_weight(psffile, outfile=None):
    """ Compute weights from PSF model supplied and save to 'outfile'. 
        Inverse weight by square of PSF at at 68% containment (inverse variance). """
    # Open file
    psf = fits.open(psffile)['PSF'].data
    thetas = fits.open(psffile)['THETA'].data['Theta']
    # Create array
    weight_array = np.empty(len(psf['ENERGY']), dtype=[('ENERGY', 'f'), ('WEIGHTS', 'f')])
    weight_array['ENERGY'] = psf['ENERGY']
    std = np.array([get_psf(psffile, e, 0.68) for e in psf['ENERGY']])
    weight_array['WEIGHTS'] = std**-2
    if outfile is not None:
        np.savez(outfile, psf_weights=weight_array)
    else:
        return weight_array

def brute_force_wrapper(args):
    """ Wrapper function to pass arguments to brute_force when running in 
        multiprocessing.Pool. """
    return brute_force(*args)

def brute_force(events, ind, fname, psf_weights, job_ind=0, collapse=True):
    """ Loop through every triplet of supplied events and compute their angular 
        separations, skipping those with differences >MAX_ANG degrees in 
        declination and auto-differences. Save results to file.

        Parameters
        ------------
        events: np.recarray
            Mulicolumn array of *all* events, the subset defined by 'ind' of 
            which will be used as first element for pair distance computations.
        ind: list of int
            Indices of first of pair of events to be considered by this job.
        fname: string
            Filename to save results to, in FITS format.
        psf_weights: np.recarray( [('ENERGY', float), ('WEIGHTS', float)] )
            Standard deviation of PSF for a set of energies.
        job_ind: int
            The index associated with this job if the full calculation has been 
            split into multiple parallel jobs.
        collapse: boolean
            Wether or not to 'collapse' all triplets associated with one event
            into a single averaged cluster, and record only this.

        Returns
        ---------
        int:
            Number of triplets saved.
        Exception:
            If an exception occurs in the main loop it will be passed on along 
            with the results up to that point.
    """
    # Create FITS file
    # Create binary HDU; J: i4, E: f4  Don't think this is necessary. Try without
    #tbhdu = fits.BinTableHDU.from_columns([ fits.Column(name='dist', format='E'), 
    #                                         fits.Column(name='evid', format='2J')])
    # Create header
    prihdr = fits.Header()
    prihdr['JOB_IND'] = job_ind
    prihdr['COMMENT'] = "Results of angular distance computation for triplets of "+\
                        "events above 50 GeV (for job {:d}).".format(job_ind)
    if collapse:
        prihdr['COMMENT'] = "All triplets associated with a single event were "+\
                            "collapsed into one average datapoint."
    # Add header and table to list, and create file
    thdulist = fits.HDUList([fits.PrimaryHDU(header=prihdr)])
    thdulist.writeto('{}.fits'.format(fname))

    # Load individual arrays
    ev_ra = events['RA']
    ev_dec = events['DEC']
    ev_en = events['ENERGY']
    ev_zen = events['ZENITH_ANGLE']
    ev_t = events['TIME'] 
    ev_id = events['EVENT_ID']

    print "\nStarting job {:d}.".format(job_ind)
    try:
        # Append results to FITS file every 10000000 counts 
        # (10e6 * 72 bytes ~ 720 MB)
        arr_size = 10e6
        if collapse:
            distances = np.zeros(arr_size, dtype=[('dist','f4'), 
                                            ('ev_num', 'i4')] + FERMI_DTYPE_ALT)
        else:
            distances = np.empty(arr_size, dtype=[('dist','f4')] + FERMI_DTYPE)
        n = 0  # Total result counter
        arr_ind = 0  # Index of results array
        ev_len = len(ev_id)
        # Loop over every pair
        for i in ind:
            if collapse:
                # initialize array for triplets from this event. estimate max size
                i_coords = np.empty(5000, dtype=[('RA', 'f4'), ('DEC', 'f4')])
                ev_arr_ind = 0
            for j in range(i+1, ev_len):  # Skip autos
                ra_pair = (ev_ra[i], ev_ra[j])
                dec_pair = (ev_dec[i], ev_dec[j])
                dij = get_ang_dist(ra_pair, dec_pair)
                if dij is None or dij > MAX_ANG:  # Skip if >MAX_ANG
                    continue
                for k in range(j+1, ev_len):
                    ra_tri = ra_pair + (ev_ra[k],)
                    dec_tri = dec_pair + (ev_dec[k],)
                    dik = get_ang_dist(ra_tri[::2], dec_tri[::2])
                    if dik is None or dik > MAX_ANG:  # Skip if >MAX_ANG
                        continue
                    djk = get_ang_dist(ra_tri[1:], dec_tri[1:])
                    if djk is None or djk > MAX_ANG:  # Skip if >MAX_ANG
                        continue
                    energies = [ev_en[i], ev_en[j], ev_en[k]]
                    new_dist = weighted_scale((dij,dik,djk), energies, psf_weights)
                    new_coord = weighted_coord(ra_tri, dec_tri, energies, psf_weights)
                    if collapse:
                        distances[arr_ind]['dist'] += new_dist
                        distances[arr_ind]['ENERGY'] += np.mean(energies)
                        i_coords[ev_arr_ind] = (new_coord[0], new_coord[1])
                        ev_arr_ind += 1
                    else:
                        distances[arr_ind] = (new_dist, new_coord[0], new_coord[1],
                                              energies, 
                                              [ev_zen[i], ev_zen[j], ev_zen[k]], 
                                              [ev_t[i], ev_t[j], ev_t[k]], 
                                              [ev_id[i], ev_id[j], ev_id[k]])
                        arr_ind += 1  # Increment results array cell
                    n += 1  # Increment total
            if collapse and ev_arr_ind > 0: # average triplets associated with this event
                avg_coord = weighted_coord(i_coords[:ev_arr_ind]['RA'], 
                                           i_coords[:ev_arr_ind]['DEC'])
                distances[arr_ind]['dist'] = distances[arr_ind]['dist'] / ev_arr_ind
                distances[arr_ind]['ev_num'] = ev_arr_ind
                distances[arr_ind]['ENERGY'] = distances[arr_ind]['ENERGY'] / ev_arr_ind
                distances[arr_ind]['RA'] = avg_coord[0]
                distances[arr_ind]['DEC'] = avg_coord[1]
                distances[arr_ind]['EVENT_ID'] = ev_id[i]
                arr_ind += 1  # Increment results array cell
            if arr_ind == arr_size:  # Array is full: write to file
                fits.append('{}.fits'.format(fname), data=distances, verify=False)
                # Reset array
                arr_ind = 0
        # Write last, partially filled, results array
        fits.append('{}.fits'.format(fname), data=distances[:arr_ind], verify=False)

        print "\nJob {:d}: Done!".format(job_ind)
    except Exception as e:
        print "\nJob {:d}: An exception occured:".format(job_ind)
        traceback.print_exc()
        print "\nJob {:d}: Computed {:d} angular distances.".format(job_ind, n)
        # Write last results
        fits.append('{}.fits'.format(fname), data=distances[:arr_ind], verify=False)
        print "\nJob {:d}: Results saved to {}.".format(job_ind, '{}.fits'.format(fname))
        return n, e

    print "\nJob {:d}: Computed {:d} angular distances.".format(job_ind, n)
    print "Job {:d}: Results saved to {}.".format(job_ind, '{}.fits'.format(fname))
    return n, None

def iteration(events, ind, fname, iteration, job_ind=0):
    """ Modified version of 'brute_force' designed to be run iteratively on the 
        resulting averaged triplets to narrow in on clustering. 

        Parameters
        ------------
        events: np.recarray
            Mulicolumn array of *all* events, the subset defined by 'ind' of 
            which will be used as first element for pair distance computations.
        ind: list of int
            Indices of first of pair of events to be considered by this job.
        fname: string
            Filename to save results to, in FITS format.
        iteration: int
            The number of iterations that preceded this one.
        job_ind: int
            The index associated with this job if the full calculation has been 
            split into multiple parallel jobs.

        Returns
        ---------
        int:
            Number of triplets saved.
        list of int:
            Indices of clusters that were not found to be part of any triplet 
            in this iteration.
        Exception:
            If an exception occurs in the main loop it will be passed on along 
            with the results up to that point.
        """
    # Create header
    prihdr = fits.Header()
    prihdr['JOB_IND'] = job_ind
    prihdr['ITER'] = iteration
    prihdr['COMMENT'] = ("Results of iterative triplets clustering of "+
                        "events above 50 GeV (for iteration {:d}, " +
                        "job {:d}).").format(iteration, job_ind)
    # Add header and table to list, and create file
    thdulist = fits.HDUList([fits.PrimaryHDU(header=prihdr)])
    thdulist.writeto('{}.fits'.format(fname))

    # Load individual arrays
    ev_dist = events['dist']
    ev_num = events['ev_num']
    ev_ra = events['RA']
    ev_dec = events['DEC']
    ev_en = events['ENERGY']
    ev_id = events['EVENT_ID']

    print "\nStarting job {:d}.".format(job_ind)
    try:
        # Output will be no larger than the input array
        arr_size = events.shape[0]
        distances = np.zeros(arr_size, dtype=[('dist','f4'), 
                                            ('ev_num', 'i4')] + FERMI_DTYPE_ALT)
        not_in_trip = np.ones(arr_size)  # keep track of events not isolated
        n = 0  # Total result counter
        arr_ind = 0  # Index of results array
        ev_len = len(ev_id)
        # Loop over every pair
        for i in ind:
            # initialize array for triplets from this event. estimate max size
            i_coords = np.empty(50000, dtype=[('RA', 'f4'), ('DEC', 'f4')])
            ev_arr_ind = 0
            for j in range(i+1, ev_len):  # Skip autos
                ra_pair = (ev_ra[i], ev_ra[j])
                dec_pair = (ev_dec[i], ev_dec[j])
                dij = get_ang_dist(ra_pair, dec_pair)
                if dij is None or dij > MAX_ANG:  # Skip if >MAX_ANG
                    continue
                for k in range(j+1, ev_len):
                    ra_tri = ra_pair + (ev_ra[k],)
                    dec_tri = dec_pair + (ev_dec[k],)
                    dik = get_ang_dist(ra_tri[::2], dec_tri[::2])
                    if dik is None or dik > MAX_ANG:  # Skip if >MAX_ANG
                        continue
                    djk = get_ang_dist(ra_tri[1:], dec_tri[1:])
                    if djk is None or djk > MAX_ANG:  # Skip if >MAX_ANG
                        continue
                    energies = [ev_en[i], ev_en[j], ev_en[k]]
                    # Events were already weighted in first pass
                    new_dist = (dij + dik + djk) / 3.
                    new_coord = weighted_coord(ra_tri, dec_tri)  # no weights
                    # Add to total for this event
                    distances[arr_ind]['dist'] += new_dist
                    distances[arr_ind]['ENERGY'] += np.mean(energies)
                    distances[arr_ind]['ev_num'] += 1
                    i_coords[ev_arr_ind] = (new_coord[0], new_coord[1])
                    # Flag these events as accounted for
                    not_in_trip[i] = 0
                    not_in_trip[j] = 0
                    not_in_trip[k] = 0
                    # Increment
                    ev_arr_ind += 1  # index
                    n += 1  # total
            if ev_arr_ind > 0:
                # average triplets associated with this event
                avg_coord = weighted_coord(i_coords[:ev_arr_ind]['RA'], 
                                           i_coords[:ev_arr_ind]['DEC'])
                distances[arr_ind]['dist'] = distances[arr_ind]['dist'] / ev_arr_ind
                distances[arr_ind]['ENERGY'] = distances[arr_ind]['ENERGY'] / ev_arr_ind
                distances[arr_ind]['RA'] = avg_coord[0]
                distances[arr_ind]['DEC'] = avg_coord[1]
                distances[arr_ind]['EVENT_ID'] = ev_id[i]
                arr_ind += 1  # Increment results array cell
            if arr_ind == arr_size:  # Array is full: write to file
                fits.append('{}.fits'.format(fname), data=distances[:arr_ind], verify=False)
                raise Exception("Overflowed results array! Something is wrong" +
                                ", number of results should be decreasing. " +
                                "Saved results to file.")
        # Write last, partially filled, results array
        fits.append('{}.fits'.format(fname), data=distances[:arr_ind], verify=False)

        print "\nJob {:d}: Done!".format(job_ind)
    except Exception as e:
        print "\nJob {:d}: An exception occured:".format(job_ind)
        traceback.print_exc()
        print "\nJob {:d}: Down to {:d} clusters.".format(job_ind, n)
        # Write last results
        fits.append('{}.fits'.format(fname), data=distances[:arr_ind], verify=False)
        print "\nJob {:d}: Results saved to {}.".format(job_ind, '{}.fits'.format(fname))
        return n, not_in_trip, e

    print "\nJob {:d}: Down to {:d} clusters.".format(job_ind, arr_ind)
    print "Job {:d}: Results saved to {}.".format(job_ind, '{}.fits'.format(fname))
    return n, not_in_trip, None

if __name__ == '__main__':
    """ Main routine for a single triplets run (with collapsing into clusters). 
        Get input and output paths, as well as psf weigths file, 
        from arguments and split computation into a given number of jobs. 

        Requires first argument to be the input events FITS file, and will use 
        second argument as output filename, if specified. """
    args = sys.argv
    if len(args) < 2:
        raise Exception("Must supply path to events FITS file.")
    elif not os.path.isfile(args[1]):
        print "File '{}' does not exist.".format(args[1])
    else:
        # Read file
        evfile = args[1]
        print "\nReading from events file '{}'".format(evfile)
        events = np.array(fits.open(evfile)['EVENTS'].data)
        ev_len = events.shape[0]
        print "\nWill compute distances for triplets from {:d} events\n".format(ev_len)
        # Set up jobs
        job_num = 8
        errors = []
        iter_per_job = ev_len / job_num
        pool = multiprocessing.Pool(processes=job_num)  # Process pool
        # Distribute indices evenly
        base_ind = np.arange(0, ev_len - job_num, job_num)
        indices = [ base_ind + i for i in range(job_num) ]
        indices[0] = np.concatenate( (indices[0], 
                                      np.arange(indices[-1][-1]+1, ev_len)) ) 
        del base_ind

        # Parse output filename
        if len(args) < 3:
            raise Exception("Insufficient number of arguments.\nRequire events " +
                "file and output filename")
        basename = os.path.splitext(args[2])[0]
        if not os.path.exists(os.path.dirname(basename)):
            try:
                os.makedirs(os.path.dirname(basename))
            except Exception as e:
                traceback.print_exc()
                print "Carrying on..."
        fnames = [ "{}_{:d}".format(basename, i) for i in range(job_num) ]

        # Load PSF weights
        if len(args) < 4:
            print "No PSF weights file specified. Will not weight by PSF."
            # Create dummy PSF weights
            psf_weights = np.ones(1, dtype=[('ENERGY', 'f'), ('WEIGHTS', 'f')])
        elif not os.path.isfile(args[3]):
            raise Exception("PSF weights file {} does not exist.".format(args[3]))
        else:
            psf_weights = np.load(args[3])['psf_weights']

        proc_args = [ (events, indices[i], fnames[i], psf_weights, i) for i in range(job_num) ]
        #   Note: passing the same arrays to each job should not result in 
        #   multiple copies in memory on linux (I think).
        # Run jobs
        job_out = pool.map(brute_force_wrapper, proc_args)
        counts = [x[0] for x in job_out]
        errors = [x[1] for x in job_out]

        # Merge all results into one file
        # Create header
        prihdr = fits.Header()
        prihdr['COMMENT'] = "Results of angular distance computation for triplets of "+\
                            "events above 50 GeV, with separation less than" +\
                            " {:.2f} deg.".format(MAX_ANG)
        prihdr['COMMENT'] = "All triplets associated with a single event were "+\
                            "collapsed into one average datapoint."
        prihdr['COMMENT'] = "Generated by Tristan Pinsonneault-Marotte, for " +\
                            "the course PHYS 459 at McGill university, under " +\
                            "the supervision of Prof. Ken Ragan."
        prihdr['DATE'] = str(datetime.now())
        # Merge tables from all files
        merged_res = np.concatenate([ part.data for f in fnames for part in 
                                    fits.open(f + '.fits')[1:] ])
        thdulist = fits.HDUList([ fits.PrimaryHDU(header=prihdr), 
                                  fits.BinTableHDU(merged_res) ])
        # Write to file
        thdulist.writeto('{}.fits'.format(basename))

        # Remove indiviual job files
        os.system("rm {}_[0-9].fits".format(basename))

        # Save result counts
        np.savez('{}_counts.npz'.format(basename), counts=np.array(counts))

        # Check for exceptions in previous steps
        for i, error in enumerate(errors):
            if error is not None:
                print "\nJob {:d} ran into an exception:".format(i)
                print error

