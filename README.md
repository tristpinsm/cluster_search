# README #

This code was used to perform a search for clustering of photon events in Fermi
data, as part of the PHYS 459 course at McGill.

### Structure ###

The code is grouped into files that roughly correspond to steps of the analysis.
Functions were designed to be somewhat generic, but they are implemented as 
employed for this project in the '\__main__' section of each file.
The python files can therefore be run as scripts, which will reproduce the 
analysis I went through, but these scripts can also be used as examples for 
other applications.

It may be necessary to look at these '\__main__' sections to identify the proper 
command line arguments to use when invoking them as scripts.

### Setup ###

I used Python 2.7, and the dependencies are essentially numpy and astropy.

Steps for obtaining the Fermi data files and performing the initial processing 
are provided at the [Fermi website](http://fermi.gsfc.nasa.gov/ssc/data/analysis/scitools/LAT_weekly_allsky.html).
This code will work on FITS files of events that use the Fermi data format, with
whatever energy, location, zenith angle, etc. cuts desired.

Most scripts were designed to be run in multiple processes in parallel, and were
hardcoded to split the task into 8 jobs. If this is not appropriate, the number 
will have to be changed directly in the code (the 'job_num' variable).

### Summary of scripts ###

#### brute_force.py ####

* Computes distances between pairs of events. 
* Command line args: input filename, output filename prefix

#### triplets.py ####

* Computes distances between triplets of events.
* Run as a script, this file performs the triplets computation, but it also 
contains the code to perform the iterative search, implemented in a separate 
script 'iterate_clustering.py'.
* Command line args: input filename, output filename prefix, 
psf weights filename (optional)
* The PSF weights file can be generated using the 'get_psf_weight' function, 
which works on a PSF model obtained from the Fermi science tools utility 'gtpsf'.

#### iterate_clustering.py ####

* Script for performing the iterative cluster search.
* Takes as input the file containing the triplets computed with the previous 
script (with 'collapse=True'), and iteratively identifies triplets among those.
* Command line args: input filename, output filename prefix

#### mask_gal.py ####

* Utility script for masking the galaxy (or other regions) from a FITS file of 
events.

#### mask_sources.py ####

* Utility script for masking a list of sources from a FITS file of events.

#### merge_fits.py ####

* Utility script for merging a list of FITS files with same column format.

#### partition_sky.py ####

* Utility script for sorting events from a FITS file into partitions on the sky 
(defined in the '\__main__' section).

### Example analysis steps ###

The following list of steps corresponds to a condensed version of the  analysis 
I used for the clustering search.

1. Input file is 'lat_gt50.fits', with the appropriate cuts made using the Fermi 
science tools.
2. Generate PSF weights, using a model 'psf_model.fits' obtained from the Fermi 
science tools command line utility 'gtpsf'.

        python -c "from triplets import get_psf_weight; get_psf_weight('psf_model.fits', 'psf_weights.npz')"

2. Mask out bright sources. (This uses by default the list of sources 
'fhl_srcs.txt' provided in this repository)

        python mask_sources.py lat_gt50.fits lat_gt50_2fhlmask.fits psf_weights.npz

2. Run the triplets computation, saving to a new file 'trip_gt50.fits', and using 
the psf weights generated previously.

        python triplets.py lat_gt50_2fhlmask.fits trip_gt50 psf_weights.npz

3. Run the iterative clustering, saving the results to 'cl_gt50.fits'.

        python iterate_clustering.py trip_gt50.fits cl_gt50.fits