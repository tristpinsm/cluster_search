""" Script for merging clusters from results of overlapping partitionings of the sky. """
import numpy as np
from astropy.io import fits
import sys
import os
from brute_force import get_ang_dist
# Distance for two clusters to be considered a match
match_dist = 0.1

# Parse arguments
args = sys.argv
if not len(args) == 4:
    raise Exception("Incorrect number of arguments. Expected two input files " +
        "and one output.")
else:
    infiles = args[1:-1]
    outfile = args[-1]
if not os.path.isdir(os.path.dirname(outfile)):
    os.makedirs(os.path.dirname(outfile))

# Read clusters file
clusters = [ fits.open(f)[1].data for f in infiles ]
# Create array for merged clusters with 20% buffer
merged_cl = np.zeros(1.2*np.max((clusters[0].shape[0],clusters[1].shape[0])),
                     dtype=clusters[0].dtype)
# Loop over pairs of clusters
compare_ind = range(clusters[1].shape[0])
k = 0  # Array counter
# Note: need to convert to tuple because apparently astropy fits arrays don't behave like numpy arrays...
for i in range(clusters[0].shape[0]):
    found_match = False
    for j in compare_ind:
        if tuple(clusters[0][i]) == tuple(clusters[1][j]):
            compare_ind.pop(j);
            merged_cl[k] = tuple(clusters[0][i])
            k += 1
            found_match = True
            break
        dist = get_ang_dist((clusters[0]['RA'][i],clusters[1]['RA'][j]),
                            (clusters[0]['DEC'][i],clusters[1]['DEC'][j]))
        if dist is not None and dist <= match_dist:
            compare_ind.pop(j);
            merged_cl[k] = tuple(clusters[0][i]) # if clusters[0]['ev_num'][i] > clusters[1]['ev_num'][j] else clusters[1][j]
            k += 1
            found_match = True
            break
    if not found_match:
        merged_cl[k] = tuple(clusters[0][i])
        k += 1
# Add remaining unmatched clusters
merged_cl[k:k+len(compare_ind)] = clusters[1][compare_ind]
k += len(compare_ind)  # final filled cell

# Save results
prihdr = fits.open(infiles[0])['PRIMARY'].header
prihdr["COMMENT"] = "Merged two overlapping partitionings of the sky."
thdulist = fits.HDUList([ fits.PrimaryHDU(header=prihdr), 
                          fits.BinTableHDU(merged_cl[:k]) ])
thdulist.writeto(outfile)
print "Saved merged clusters to {}".format(outfile)




