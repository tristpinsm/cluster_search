""" This script reads a fits file and outputs a modified version with a region 
    of the sky masked. 
    Made to mask the galaxy, but could reuse structure."""
from astropy.io import fits
import numpy as np
import sys
import os

GAL_LIMITS = { 'l_bulge': 30.,
               'l_disk': 120.,
               'b_bulge': 10.,
               'b_disk': 5. }
mask_corner = None  

def set_gal_lim(l_bulge, l_disk, b_bulge, b_disk):
    GAL_LIMITS.update( { 'l_bulge': l_bulge,
                         'l_disk': l_disk,
                         'b_bulge': b_bulge,
                         'b_disk': b_disk } )

def lb_gal_cstrnt(l, b):
    """ Constraint on galactic longitude and lattitude
        Returns True or False, whether to keep or mask. """
    if l < GAL_LIMITS['l_bulge'] or 360 - l < GAL_LIMITS['l_bulge']:
        return not np.abs(b) < GAL_LIMITS['b_bulge']
    elif l < GAL_LIMITS['l_disk'] or 360 - l < GAL_LIMITS['l_disk']:
        result = not np.abs(b) < GAL_LIMITS['b_disk']
        if not result: return result
    if mask_corner is not None:
        if mask_corner == 'tl':
            return not (l > 180 and b > 0 and b < GAL_LIMITS['b_bulge'])
        elif mask_corner == 'tr':
            return not (l < 180 and b > 0 and b < GAL_LIMITS['b_bulge'])
        elif mask_corner == 'bl':
            return not (l > 180 and b < 0 and b > -1*GAL_LIMITS['b_bulge'])
        elif mask_corner == 'br':
            return not (l < 180 and b < 0 and b > -1*GAL_LIMITS['b_bulge'])
    return True

def lb_bump_search_cstrnt(l, b):
    """ Used this function to iteratively isolate source of '3 deg bump'.
        Note that the values hardcoded here should be matched to the 'GAL_LIMITS'
        parameters being used or modified."""
    unmasked = lb_gal_cstrnt(l, b)
    # mask 'left' half of bottom left corner
    if unmasked:
        return not (l > 180 and l < 191.25 and b < 0 and b > -1*GAL_LIMITS['b_bulge'])
    else:
        return unmasked

def mask_gal(infile, outfile, lb_cstrnt):
    """ Make a copy of 'infile' with galaxy masked specified by function 'lb_cstrnt' and save to 'outfile' """
    print "\nReading from {}".format(infile)
    # Read input file, copy header 
    infits = fits.open(infile)
    prihdu = infits['PRIMARY']
    prihdu.header['FILENAME'] = os.path.split(outfile)[-1]
    gti = infits['GTI']
    events = infits['EVENTS']
    new_data = fits.fitsrec.FITS_rec.from_columns(events.columns, fill=True)  # Fill with zeros
    # Filter events according to mask constraint
    print "\nFiltering events..."
    k = 0  # initialize counter
    for i in range(events.data.shape[0]):
        if lb_cstrnt(events.data['L'][i], events.data['B'][i]):
            new_data[k] = events.data[i]
            k += 1
    print "Masking completed."
    print "{:d} events remain of the initial {:d}".format(k, events.data.shape[0])
    # Discard empty cells
    new_data = new_data[:k]
    # Create new events table
    new_events = fits.BinTableHDU(new_data)
    new_events.header = events.header
    new_events.name = events.name
    # Record this edit to file history
    new_events.header['HISTORY'] = "Rough mask applied to galaxy."
    # Write to file
    hdulist = fits.HDUList( [prihdu, new_events, gti] )
    hdulist.writeto(outfile)

    print "\nSaved new file {}".format(outfile)

if __name__ == '__main__':
    if len(sys.argv) > 3:
        if len(sys.argv) == 7:
            GAL_LIMITS = { 'l_bulge': float(sys.argv[3]),
                           'l_disk': float(sys.argv[4]),
                           'b_bulge': float(sys.argv[5]),
                           'b_disk': float(sys.argv[6]) }
            mask_gal(sys.argv[1], sys.argv[2], lb_gal_cstrnt)
        elif len(sys.argv) == 8:
            GAL_LIMITS = { 'l_bulge': float(sys.argv[3]),
                           'l_disk': float(sys.argv[4]),
                           'b_bulge': float(sys.argv[5]),
                           'b_disk': float(sys.argv[6]) }
            mask_corner = sys.argv[7]
            mask_gal(sys.argv[1], sys.argv[2], lb_gal_cstrnt)
        else: 
            print "Too many arguments to use default galaxy limits, but not enough" +\
                  " to interpret as specified galaxy limits!"
    else:
        mask_gal(sys.argv[1], sys.argv[2])

